package com.example.huypt1.buoi10.view;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.huypt1.buoi10.R;
import com.example.huypt1.buoi10.adapter.StoryAdapter;
import com.example.huypt1.buoi10.model.StoryItem;

import java.util.ArrayList;
import java.util.List;

public class StoryActivity extends AppCompatActivity {
    private List<StoryItem> storyItems;
    private StoryAdapter storyAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        createStorys();

        storyAdapter = new StoryAdapter(this, storyItems);
        recyclerView.setAdapter(storyAdapter);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
    }

    private void initComponents() {
        recyclerView = (RecyclerView) findViewById(R.id.rcv_story);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void createStorys() {
        storyItems = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            storyItems.add(new StoryItem(
                    R.drawable.bg_story,
                    " Chiến Tranh Và Hòa Bình",
                    "Lev Tolstoy",
                    "Tiểu thuyết, Hư cấu lịch sử",
                    "600",
                    "Chương 59",
                    "2015-08-33 08:34:14",
                    "2017-10-12 18:06:22",
                    R.string.content_ctvhb));

            storyItems.add(new StoryItem(
                    R.drawable.bg_laohac,
                    "Lão Hạc",
                    "Nam Cao",
                    "Truyện ngắn, văn học Việt Nam",
                    "5",
                    "Chương 1",
                    "2012-05-19 11:44:01",
                    "2014-10-07 15:03:29",
                    R.string.content_laohac));

            storyItems.add(new StoryItem(
                    R.drawable.bg_langvudai,
                    "Chí Phèo - Làng Vũ Đại Ngày Ấy",
                    "Nam Cao",
                    "Truyện ngắn, văn học Việt Nam",
                    "8",
                    "Chương 5",
                    "2011-05-12 17:21:09",
                    "2013-03-07 09:21:18",
                    R.string.content_chipheo));
        }
    }
}
