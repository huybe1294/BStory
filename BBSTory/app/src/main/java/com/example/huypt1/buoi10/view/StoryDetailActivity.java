package com.example.huypt1.buoi10.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.huypt1.buoi10.R;
import com.example.huypt1.buoi10.model.StoryItem;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by huypt1 on 10/27/2017.
 */

public class StoryDetailActivity extends AppCompatActivity {
    private CircleImageView imgAvatar;
    private TextView txtStoryName;
    private TextView txtAuthor;
    private TextView txtCategory;
    private TextView txtTotalChapter;
    private TextView txtTimeUp;
    private TextView txtTimeUpdate;
    private TextView txtContent;

    private StoryItem storyItem;

    private ImageView btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);

        initComponents();
    }

    private void initComponents() {
        imgAvatar = (CircleImageView) findViewById(R.id.img_avatar);
        txtStoryName = (TextView) findViewById(R.id.txt_story_name);
        txtAuthor = (TextView) findViewById(R.id.txt_author);
        txtCategory = (TextView) findViewById(R.id.txt_category);
        txtTotalChapter = (TextView) findViewById(R.id.txt_total_chapter);
        txtTimeUp = (TextView) findViewById(R.id.txt_time_up);
        txtTimeUpdate = (TextView) findViewById(R.id.txt_time_update);
        txtContent = (TextView) findViewById(R.id.txt_content);

        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), StoryActivity.class));
            }
        });

        //receive data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            storyItem = (StoryItem) getIntent().getSerializableExtra(Key.STORY);
        }

        imgAvatar.setImageResource(storyItem.getAvatar());
        txtStoryName.setText(storyItem.getStoryName());
        txtAuthor.setText(storyItem.getAuthor());
        txtCategory.setText(storyItem.getCategory());
        txtTotalChapter.setText(storyItem.getTotalChapter());
        txtTimeUp.setText(storyItem.getTimeUp());
        txtTimeUpdate.setText(storyItem.getTimeUpdate());
        txtContent.setText(storyItem.getContent());
    }

}
