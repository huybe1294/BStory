package com.example.huypt1.buoi10.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.huypt1.buoi10.view.Key;
import com.example.huypt1.buoi10.R;
import com.example.huypt1.buoi10.model.StoryItem;
import com.example.huypt1.buoi10.view.StoryDetailActivity;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by huypt1 on 10/27/2017.
 */

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {
    private List<StoryItem> storyItems;
    private Context context;
    private LayoutInflater inflater;

    public StoryAdapter() {
    }

    public StoryAdapter(Context context, List<StoryItem> storyItems) {
        this.storyItems = storyItems;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_story, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StoryItem item = storyItems.get(position);

        holder.avatar.setImageResource(item.getAvatar());
        holder.storyName.setText(item.getStoryName());
        holder.chapter.setText(item.getChapter());
        holder.author.setText(item.getAuthor());
        holder.timeUpdate.setText(item.getTimeUpdate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, StoryDetailActivity.class);
                intent.putExtra(Key.STORY, item);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return storyItems.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView avatar;
        private TextView storyName;
        private TextView chapter;
        private TextView author;
        private TextView timeUpdate;

        public ViewHolder(View itemView) {
            super(itemView);

            avatar = (CircleImageView) itemView.findViewById(R.id.img_avatar);
            storyName = (TextView) itemView.findViewById(R.id.txt_story_name);
            chapter = (TextView) itemView.findViewById(R.id.txt_chapter);
            author = (TextView) itemView.findViewById(R.id.txt_author);
            timeUpdate = (TextView) itemView.findViewById(R.id.txt_time_update);
        }
    }
}
