package com.example.huypt1.buoi10.model;

import java.io.Serializable;

/**
 * Created by huypt1 on 10/27/2017.
 */

public class StoryItem  implements Serializable{
    private int avatar;
    private String storyName;
    private String author;
    private String category;
    private String totalChapter;
    private String chapter;
    private String timeUpdate;
    private String timeUp;
    private int content;

    public StoryItem() {
    }

    public StoryItem(int avatar, String storyName, String author, String category,
                     String totalChapter, String chapter,String timeUp, String timeUpdate, int content) {
        this.avatar = avatar;
        this.storyName = storyName;
        this.author = author;
        this.category = category;
        this.totalChapter = totalChapter;
        this.chapter = chapter;
        this.timeUpdate = timeUpdate;
        this.timeUp = timeUp;
        this.content=content;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getStoryName() {
        return storyName;
    }

    public void setStoryName(String storyName) {
        this.storyName = storyName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTotalChapter() {
        return totalChapter;
    }

    public void setTotalChapter(String totalChapter) {
        this.totalChapter = totalChapter;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getTimeUpdate() {
        return timeUpdate;
    }

    public void setTimeUpdate(String timeUpdate) {
        this.timeUpdate = timeUpdate;
    }

    public String getTimeUp() {
        return timeUp;
    }

    public void setTimeUp(String timeUp) {
        this.timeUp = timeUp;
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }
}
